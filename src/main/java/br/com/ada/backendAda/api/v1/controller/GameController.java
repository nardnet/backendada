package br.com.ada.backendAda.api.v1.controller;

import br.com.ada.backendAda.api.service.GameService;
import br.com.ada.backendAda.api.service.MovieService;
import br.com.ada.backendAda.api.service.RoundService;
import br.com.ada.backendAda.api.service.UserService;
import br.com.ada.backendAda.api.v1.dto.GameDTO;
import br.com.ada.backendAda.api.v1.dto.RoundDTO;
import br.com.ada.backendAda.api.v1.dto.UserDTO;
import br.com.ada.backendAda.api.v1.openapi.controller.GameControllerOpenApi;
import br.com.ada.backendAda.domain.model.Game;
import br.com.ada.backendAda.domain.model.Movie;
import br.com.ada.backendAda.domain.model.Round;
import br.com.ada.backendAda.domain.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/games")
public class GameController implements GameControllerOpenApi {



    @Autowired
    private GameService gameService;
    @Autowired
    private UserService userService;
    @Autowired
    private RoundService roundService;

    @Autowired
    private MovieService movieService;

    @Autowired
    private ModelMapper modelMapper;


    @PostMapping("/start")
    public ResponseEntity<?> startGame(@RequestParam String username) {
        User user = userService.findByEmail(username);
        if (user == null) {
            return ResponseEntity.badRequest().body("User not found");
        }
        Game game = gameService.startNewGame(user);

        // Converte o game para GameDTO
        GameDTO gameDTO = modelMapper.map(game, GameDTO.class);
        return ResponseEntity.ok(gameDTO);
    }

    @PostMapping("/end")
    public ResponseEntity<?> endGame(@RequestParam Long gameId) {
        Optional<Game> gameOptional = Optional.ofNullable(gameService.findById(gameId));
        if (gameOptional.isEmpty()) {
            return ResponseEntity.badRequest().body("Game not found");
        }
        Game game = gameOptional.get();
        gameService.endGame(game);
        return ResponseEntity.ok("Game Ended");
    }

    @GetMapping("/round")
    public ResponseEntity<?> getNewRound(@RequestParam Long gameId) {
        Optional<Game> gameOptional = Optional.ofNullable(gameService.findById(gameId));
        if (gameOptional.isEmpty()) {
            return ResponseEntity.badRequest().body("Game not found");
        }
        Game game = gameOptional.get();
        Round round = roundService.findByGameId(game.getId());

        if (Objects.nonNull(round) && !round.isAnswered()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        round = roundService.createNewRound(game);
        // Converte o round para RoundDTO
        RoundDTO roundDTO = modelMapper.map(round, RoundDTO.class);
        return ResponseEntity.ok(roundDTO);
    }

    @PostMapping("/round/answer")
    public ResponseEntity<?> answerRound(@RequestParam Long roundId, @RequestParam Long movieId, @RequestParam String username) {

        User user = userService.findByEmail(username);
        if (user == null) {
            return ResponseEntity.badRequest().body("User not found");
        }


        Optional<Round> roundOptional = Optional.ofNullable(roundService.findById(roundId));
        if (roundOptional.isEmpty()) {
            return ResponseEntity.badRequest().body("Round not found");
        }
        Round round = roundOptional.get();

        Optional<Movie> movieOptional = Optional.ofNullable(movieService.findById(movieId));
        if (movieOptional.isEmpty()) {
            return ResponseEntity.badRequest().body("Movie not found");
        }
        Movie selectedMovie = movieOptional.get();

        boolean correct = roundService.answerRound(round, selectedMovie);
        round.setAnswered(true);
        round.setCorrect(correct);

        if (correct) {
            user.setCorrectQuizzes(Objects.isNull(user.getCorrectQuizzes())? 1 : user.getCorrectQuizzes() + 1);
        }

        user.setTotalQuizzes(Objects.isNull(user.getTotalQuizzes())? 1 : user.getTotalQuizzes() + 1);
        userService.save(user);

        roundService.save(round);

        return ResponseEntity.ok(correct ? "Correct" : "Wrong");
    }

    @GetMapping("/ranking")
    public ResponseEntity<List<UserDTO>> getRanking() {
        List<User> ranking = userService.getRanking();

        // Ordena os usuários com base em suas pontuações (do maior para o menor)
        ranking.sort(Comparator.comparingDouble(User::getScore).reversed());

        // Converte a lista de User para UserDTO
        List<UserDTO> rankingDTO = ranking.stream()
                .map(user -> modelMapper.map(user, UserDTO.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok(rankingDTO);
    }
}
