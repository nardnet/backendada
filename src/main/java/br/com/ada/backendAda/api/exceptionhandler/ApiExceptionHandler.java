package br.com.ada.backendAda.api.exceptionhandler;


import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import br.com.ada.backendAda.domain.exception.EntityNotFoundException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.validation.BindException;

import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;

/**
 * RFC 7807
 */
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    public static final String ERROR_SINTAX = "api.exception.handler.error.syntax";
    public static final String INTERNAL_SERVER_ERROR_MSG = "api.exception.handler.internal.server.error.msg";

    @Autowired
    private MessageSource messageSource;


    protected  ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status,
                                                      WebRequest request) {

        return handleValidationInternal(ex, headers, status, request, ex.getBindingResult());
    }


    @ExceptionHandler({ ValidationException.class })
    public ResponseEntity<Object> handleValidacaoException(ValidationException ex, WebRequest request) {
        return handleValidationInternal(ex, new HttpHeaders(), HttpStatus.BAD_REQUEST, request, ex.getBindingResult());
    }


    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleValidationInternal(ex, headers, status, request, ex.getBindingResult());
    }

    private ResponseEntity<Object> handleValidationInternal(Exception ex, HttpHeaders headers,
                                                            HttpStatus status, WebRequest request, BindingResult bindingResult) {

        ApiErrorType problemType = ApiErrorType.INVALID_DATA;
        String detail = messageSource.getMessage("api.exception.handler.handle.method.argument.not.valid.detail", null,
                LocaleContextHolder.getLocale());

        List<ApiError.Object> problemFields = bindingResult.getAllErrors().stream().map(objectError -> {
            String message = messageSource.getMessage(objectError, LocaleContextHolder.getLocale());

            String name = objectError.getObjectName();

            if (objectError instanceof FieldError) {
                name = ((FieldError) objectError).getField();
            }

            ApiError.Object apiErrorField = new ApiError.Object();
            apiErrorField.setName(name);
            apiErrorField.setUserMessage(message);
            return apiErrorField;
        }).collect(Collectors.toList());

        ApiError problem = createApiErrorWithUserMessageBuilder(status, problemType, detail);
        problem.setObjects(problemFields);

        return handleExceptionInternal(ex, problem, headers, status, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleUncaught(Exception ex, WebRequest request) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ApiErrorType problemType = ApiErrorType.INTERNAL_SERVER_ERROR;

        // ex.printStackTrace();
        ApiError problem = createApiErrorBuilder(status, problemType,
                messageSource.getMessage(INTERNAL_SERVER_ERROR_MSG, null, LocaleContextHolder.getLocale()));

        return handleExceptionInternal(ex, problem, new HttpHeaders(), status, request);
    }


    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
                                                                   HttpStatus status, WebRequest request) {

        ApiErrorType problemType = ApiErrorType.RESOURCE_NOT_FOUND;
        String detail = String
                .format(messageSource.getMessage("api.exception.handler.no.handler.found.exception.detail", null,
                        LocaleContextHolder.getLocale()), ex.getRequestURL());

        ApiError problem = createApiErrorWithUserMessageBuilder(status, problemType, detail);

        return handleExceptionInternal(ex, problem, headers, status, request);
    }


    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
                                                        HttpStatus status, WebRequest request) {

        if (ex instanceof MethodArgumentTypeMismatchException) {
            return handleMethodArgumentTypeMismatch((MethodArgumentTypeMismatchException) ex, headers, status, request);
        }

        return super.handleTypeMismatch(ex, headers, status, request);
    }

    private ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
                                                                    HttpHeaders headers, HttpStatus status, WebRequest request) {

        ApiErrorType problemType = ApiErrorType.PARAMETER_INVALID;

        String detail = String.format(
                messageSource.getMessage("api.exception.handler.method.argument.type.mismatch", null,
                        LocaleContextHolder.getLocale()),
                ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName());

        ApiError problem = createApiErrorWithUserMessageBuilder(status, problemType, detail);
        return handleExceptionInternal(ex, problem, headers, status, request);
    }


    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        Throwable rootCause = ExceptionUtils.getRootCause(ex);

        if (rootCause instanceof InvalidFormatException) {
            return handleInvalidFormatException((InvalidFormatException) rootCause, headers, status, request);
        } else if (rootCause instanceof PropertyBindingException) {
            return handlePropertyBindingException((PropertyBindingException) rootCause, headers, status, request);
        }

        ApiErrorType problemType = ApiErrorType.INCOMPREHENSIBLE_MESSAGE;
        String detail = messageSource.getMessage(ERROR_SINTAX, null, LocaleContextHolder.getLocale());

        ApiError problem = createApiErrorWithUserMessageBuilder(status, problemType, detail);
        return handleExceptionInternal(ex, problem, headers, status, request);
    }

    private ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ex, HttpHeaders headers,
                                                                HttpStatus status, WebRequest request) {

        String path = ex.getPath().stream().map(Reference::getFieldName).collect(Collectors.joining("."));

        ApiErrorType problemType = ApiErrorType.INCOMPREHENSIBLE_MESSAGE;
        String detail = String.format(messageSource.getMessage("api.exception.handler.invalidFormat.exception.detail",
                null, LocaleContextHolder.getLocale()), path, ex.getValue(), ex.getTargetType().getSimpleName());

        ApiError problem = createApiErrorWithUserMessageBuilder(status, problemType, detail);
        return handleExceptionInternal(ex, problem, headers, status, request);
    }

    private ResponseEntity<Object> handlePropertyBindingException(PropertyBindingException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {

        String path = joinPath(ex.getPath());

        ApiErrorType problemType = ApiErrorType.INCOMPREHENSIBLE_MESSAGE;
        String detail = String
                .format(messageSource.getMessage("api.exception.handler.property.binding.exception.detail", null,
                        LocaleContextHolder.getLocale()), path);

        ApiError problem = createApiErrorWithUserMessageBuilder(status, problemType, detail);
        return handleExceptionInternal(ex, problem, headers, status, request);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<?> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest request) {

        HttpStatus status = HttpStatus.NOT_FOUND;
        ApiErrorType problemType = ApiErrorType.RESOURCE_NOT_FOUND;
        String detail = ex.getMessage();

        ApiError problem = createApiErrorWithUserMessageBuilder(status, problemType, detail);
        return handleExceptionInternal(ex, problem, new HttpHeaders(), status, request);
    }

    private ApiError createApiErrorWithUserMessageBuilder(HttpStatus status, ApiErrorType problemType, String detail) {
        ApiError problem = createApiErrorBuilder(status, problemType, detail);
        problem.setUserMessage(
                messageSource.getMessage(INTERNAL_SERVER_ERROR_MSG, null, LocaleContextHolder.getLocale()));
        return problem;
    }


    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
                                                             HttpStatus status, WebRequest request) {

        ApiError api = new ApiError();

        if (body == null) {
            api.setTitle(status.getReasonPhrase());
            api.setTimestamp(LocalDateTime.now());
            api.setStatus(status.value());
            api.setUserMessage(
                    messageSource.getMessage(INTERNAL_SERVER_ERROR_MSG, null, LocaleContextHolder.getLocale()));
            body = api;
        } else if (body instanceof String) {
            api.setTitle((String) body);
            api.setTimestamp(LocalDateTime.now());
            api.setStatus(status.value());
            api.setUserMessage(
                    messageSource.getMessage(INTERNAL_SERVER_ERROR_MSG, null, LocaleContextHolder.getLocale()));
            body = api;
        }

        return super.handleExceptionInternal(ex, body, headers, status, request);
    }

    private ApiError createApiErrorBuilder(HttpStatus status, ApiErrorType problemType, String detail) {

        ApiError api = new ApiError();
        api.setStatus(status.value());
        api.setType(problemType.getUri());
        api.setTitle(problemType.getTitle());
        api.setDetail(detail);
        api.setTimestamp(LocalDateTime.now());

        return api;
    }

    private String joinPath(List<Reference> references) {
        return references.stream().map(Reference::getFieldName).collect(Collectors.joining("."));
    }

}


