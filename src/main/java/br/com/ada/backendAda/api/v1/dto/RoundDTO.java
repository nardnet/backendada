package br.com.ada.backendAda.api.v1.dto;

import lombok.Data;

@Data
public class RoundDTO {


    private Long id;

    private String code;

    private GameDTO game;

    private MovieDTO movie1;

    private MovieDTO movie2;
    private boolean answered;
    private boolean correct;

}
