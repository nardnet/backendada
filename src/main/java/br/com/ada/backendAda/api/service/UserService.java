package br.com.ada.backendAda.api.service;

import br.com.ada.backendAda.api.repository.UserRepository;
import br.com.ada.backendAda.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User register(User user) {
        return userRepository.save(user);
    }

    public User findByName(String username) {
        return userRepository.findByName(username);
    }

    public User findByEmail(String username) {
        return userRepository.findByEmail(username);
    }

    public List<User> getRanking() {
        return userRepository.findAll(Sort.by(Sort.Direction.DESC, "score"));
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public double calculateScore(User user) {
        int totalQuizzes = Objects.isNull(user.getTotalQuizzes()) ? 0 : user.getTotalQuizzes();
        int correctQuizzes = Objects.isNull(user.getCorrectQuizzes()) ? 0 : user.getCorrectQuizzes();

        return (double) correctQuizzes / totalQuizzes * 100.0;
    }
}
