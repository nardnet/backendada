package br.com.ada.backendAda.api.service;

import br.com.ada.backendAda.api.repository.GameRepository;
import br.com.ada.backendAda.domain.model.Game;
import br.com.ada.backendAda.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class GameService {

    @Autowired
    private GameRepository gameRepository;

    public Game startNewGame(User user) {
        Game game = new Game();
        game.setUser(user);
        game.setCode(String.valueOf(UUID.randomUUID()));
        game.setActive(true);
        game.setCorrectAnswers(0);
        game.setWrongAnswers(0);
        return gameRepository.save(game);
    }

    public void endGame(Game game) {
        game.setActive(false);
        gameRepository.save(game);
    }

    public Game findById(Long id) {
        return gameRepository.findById(id).orElse(null);
    }
}
