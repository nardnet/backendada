package br.com.ada.backendAda.api.repository;

import br.com.ada.backendAda.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface  UserRepository extends JpaRepository<User, Long> {
    User findByName(String username);

    User findByEmail(String mail);
}
