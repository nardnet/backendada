package br.com.ada.backendAda.api.v1.dto;

import br.com.ada.backendAda.domain.model.Group;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
public class UserDTO {

    @JsonIgnore
    private Long id;

    private UUID code;

    private String name;


    private String email;

    @JsonIgnore
    private String password;

    private Integer totalQuizzes;

    private Integer correctQuizzes;

    private Double score;

    private Set<Group> groups = new HashSet<>();
}
