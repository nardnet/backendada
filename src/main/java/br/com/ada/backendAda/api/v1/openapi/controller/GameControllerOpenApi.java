package br.com.ada.backendAda.api.v1.openapi.controller;

import br.com.ada.backendAda.api.v1.dto.UserDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.ResponseEntity;

import java.util.List;

@SecurityRequirement(name = "security_auth")
@Tag(name = "Games")
public interface  GameControllerOpenApi {
    @Operation(summary = "Start a new game", description = "Starts a new game for the given user.")
    ResponseEntity<?> startGame(@Parameter(description = "Username of the user starting the game", required = true) String username);

    @Operation(summary = "End a game", description = "Ends the specified game.")
    ResponseEntity<?> endGame(@Parameter(description = "ID of the game to end", required = true) Long gameId);

    @Operation(summary = "Get a new round", description = "Gets a new round for the specified game.")
    ResponseEntity<?> getNewRound(@Parameter(description = "ID of the game to get a new round for", required = true) Long gameId);

    @Operation(summary = "Answer a round", description = "Submits an answer for the specified round.")
    ResponseEntity<?> answerRound(
            @Parameter(description = "ID of the round being answered", required = true) Long roundId,
            @Parameter(description = "ID of the movie selected as the answer", required = true) Long movieId,
            @Parameter(description = "Username of the user answering the round", required = true) String username
    );

    @Operation(summary = "Get ranking", description = "Gets the ranking of users based on their scores.")
    ResponseEntity<List<UserDTO>> getRanking();
}
