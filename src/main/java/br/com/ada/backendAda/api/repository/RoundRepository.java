package br.com.ada.backendAda.api.repository;

import br.com.ada.backendAda.domain.model.Round;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoundRepository extends JpaRepository<Round, Long> {

    Optional<Round> findByGameId(Long gameId);
}

