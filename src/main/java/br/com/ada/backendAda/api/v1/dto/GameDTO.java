package br.com.ada.backendAda.api.v1.dto;

import lombok.Data;

@Data
public class GameDTO {

    private Long id;
    private String code;
    private UserDTO user;
    private boolean active;
    private int correctAnswers;
    private int wrongAnswers;
}
