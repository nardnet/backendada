package br.com.ada.backendAda.api.service;

import br.com.ada.backendAda.api.repository.MovieRepository;
import br.com.ada.backendAda.domain.model.Movie;
import br.com.ada.backendAda.domain.service.OMDBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private OMDBService omdbService;

    public List<Movie> getRandomMovies() {

        int numberOfMovies = 10;


        List<Movie> randomMovies = new ArrayList<>();

        for (int i = 0; i < numberOfMovies; i++) {
            Movie randomMovie = omdbService.getRandomMovie();
            if (randomMovie != null) {
                randomMovies.add(randomMovie);
            }
        }

        return randomMovies;
    }

    public Movie findById(Long id) {
        return movieRepository.findById(id).orElse(null);
    }

    public Movie findByTitle(String title) {
        return movieRepository.findByTitle(title);
    }

    public Movie save(Movie movie) {
        return movieRepository.save(movie);
    }
}
