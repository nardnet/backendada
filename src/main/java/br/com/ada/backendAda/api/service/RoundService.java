package br.com.ada.backendAda.api.service;

import br.com.ada.backendAda.api.repository.RoundRepository;
import br.com.ada.backendAda.domain.model.Game;
import br.com.ada.backendAda.domain.model.Movie;
import br.com.ada.backendAda.domain.model.Round;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

@Service
public class RoundService {

    @Autowired
    private RoundRepository roundRepository;

    @Autowired
    private MovieService movieService;

    public Round createNewRound(Game game) {

        Round round = new Round();
        round.setGame(game);
        round.setCode(String.valueOf(UUID.randomUUID()));
        round.setMovie1(getRandomMovie());
        round.setMovie2(getRandomMovie());
        round.setAnswered(false);
        round.setCorrect(false);
        return roundRepository.save(round);
    }


    private Movie getRandomMovie() {
        List<Movie> movies = movieService.getRandomMovies();
        if (movies != null && !movies.isEmpty()) {
            int randomIndex = (int) (Math.random() * movies.size());
            Movie randomMovie = movies.get(randomIndex);

            // Verifique se o filme escolhido aleatoriamente já existe na base de dados
            Movie existingMovie = movieService.findByTitle(randomMovie.getTitle());

            if (existingMovie == null) {
                randomMovie.setCode(UUID.randomUUID());
                // Se não existir na base de dados, persista o filme
                existingMovie = movieService.save(randomMovie);
            }

            return existingMovie;
        }
        return null;
    }

    public boolean answerRound(Round round, Movie selectedMovie) {

        double rating1 = calculateTotalRating(round.getMovie1().getImdbRating(), round.getMovie1().getImdbVotes());
        double rating2 = calculateTotalRating(round.getMovie2().getImdbRating(), round.getMovie2().getImdbVotes());

        round.setAnswered(true);

        // Atualiza o número total de quizzes respondidos
        round.getGame().getUser().setTotalQuizzes(round.getGame().getUser().getTotalQuizzes() + 1);

        // Lógica para verificar a resposta correta
        boolean isCorrect = false;
        if (rating1 > rating2 && round.getMovie1().getId().equals(selectedMovie.getId())) {
            isCorrect = true;
        } else if (rating2 > rating1 && round.getMovie2().getId().equals(selectedMovie.getId())) {
            isCorrect = true;
        }

        // Atualiza os valores com base na resposta
        if (isCorrect) {
            round.setCorrect(true);
            round.getGame().getUser().setCorrectQuizzes(round.getGame().getUser().getCorrectQuizzes() + 1);
        } else {
            round.getGame().setWrongAnswers(round.getGame().getWrongAnswers() + 1);
        }

        // Calcula a pontuação do usuário
        double score = calculateScore(round.getGame().getUser().getTotalQuizzes(), round.getGame().getUser().getCorrectQuizzes());
        round.getGame().getUser().setScore(score);

        // Retorna verdadeiro se a resposta for correta, caso contrário, falso
        return isCorrect;
    }

    private double calculateTotalRating(String ratings, String imdbVotes) {
        // Verifica se há classificações e votos IMDb válidos
        if (ratings == null || imdbVotes == null) {
            return 0.0; // Ou algum valor padrão, dependendo do seu requisito
        }

        // Calcula a média das classificações
        double averageRating = Double.parseDouble(ratings.replace(",", ""));

        // Converte o número de votos para um valor numérico
        double votes = Double.parseDouble(imdbVotes.replace(",", "")); // Remove vírgulas, se houver

        // Calcula a classificação total multiplicando a média pela contagem de votos
        return averageRating * votes;
    }

    private double calculateScore(int totalQuizzes, int correctQuizzes) {
        if (totalQuizzes == 0) {
            return 0.0;
        }
        return (double) correctQuizzes / totalQuizzes * totalQuizzes; // pontuação = quizzes corretos / total de quizzes * total de quizzes
    }


    public Round findById(Long id) {
        return roundRepository.findById(id).orElse(null);
    }

    public Round findByGameId(Long gameId) {
        return roundRepository.findByGameId(gameId).orElse(null);
    }

    public Round save(Round round) {
        return roundRepository.save(round);
    }
}
