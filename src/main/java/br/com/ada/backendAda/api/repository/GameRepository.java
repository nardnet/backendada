package br.com.ada.backendAda.api.repository;
import br.com.ada.backendAda.domain.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository  extends JpaRepository<Game, Long> {
}