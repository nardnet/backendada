package br.com.ada.backendAda.api.exceptionhandler;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.BindingResult;

@Setter
@Getter
public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private BindingResult bindingResult;

}
