package br.com.ada.backendAda.api.exceptionhandler;

enum ApiErrorType {

    INVALID_DATA("/invalid-data", "Ivalid Data"),
    INTERNAL_SERVER_ERROR("/internal-server-error", "Internal Server Error"),
    INCOMPREHENSIBLE_MESSAGE("/incomprehensible-message", "Incomprehensible message"),
    PARAMETER_INVALID("/ivalid-parameter", "Ivalide parameter"),
    RESOURCE_NOT_FOUND("/resource-not-found", "Resource not found");


    private String title;
    private String uri;

    ApiErrorType(String path, String title) {
        this.uri = "https://ada.com.br/" + path;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }




}
