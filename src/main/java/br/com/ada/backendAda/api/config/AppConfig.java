package br.com.ada.backendAda.api.config;

import br.com.ada.backendAda.audit.AuditingService;
import br.com.ada.backendAda.audit.AuthSecutiry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {

    @Bean
    RestTemplate getRestTemplate(){
        return new RestTemplate();
    };

    @Bean
    ResourceServerConfig resourceServer() {
        return new ResourceServerConfig();
    }


    @Bean
    AuditingService auditingService() {
        return new AuditingService();

    }

    @Bean
    AuthSecutiry authSecutiry() {
        return new AuthSecutiry();
    }


}
