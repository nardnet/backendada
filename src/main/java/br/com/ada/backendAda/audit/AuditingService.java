package br.com.ada.backendAda.audit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuditingService implements AuditorAware<String> {

    @Autowired
    private AuthSecutiry authSecurity;

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.ofNullable(authSecurity.getUserId().toString());
    }

}

