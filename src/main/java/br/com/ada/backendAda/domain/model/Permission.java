package br.com.ada.backendAda.domain.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Data
@EqualsAndHashCode
@Entity(name = "aut_permission")
public class Permission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private UUID code;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

}
