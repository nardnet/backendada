package br.com.ada.backendAda.domain.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Data
@Entity(name = "ada_game")
@EqualsAndHashCode
public class Game implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "code", nullable = false)
    private String code;
    @ManyToOne
    private User user;
    private boolean active;
    private int correctAnswers;
    private int wrongAnswers;
}
