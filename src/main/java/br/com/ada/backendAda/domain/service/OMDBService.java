package br.com.ada.backendAda.domain.service;

import br.com.ada.backendAda.domain.model.Movie;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


@Slf4j
@Service
public class OMDBService {

    @Value("${omdb.api.key}")
    private String apiKey;
    private static final String OMDB_API_URL = "http://www.omdbapi.com/";

    private final RestTemplate restTemplate;

    // Lista de palavras para escolher aleatoriamente
    private static final List<String> DICTIONARY_WORDS = Arrays.asList(
            "random", "love", "happy", "fear", "fantasy", "day", "danger", "mib", "man", "moon"
    );

    @Autowired
    public OMDBService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Movie getRandomMovie() {

        // Gera uma palavra aleatória do dicionário
        String randomWord = getRandomWordFromDictionary();

        log.info("Search movie {}", randomWord);

        String url = UriComponentsBuilder.fromHttpUrl(OMDB_API_URL)
                .queryParam("apikey", apiKey)
                .queryParam("type", "movie")
                .queryParam("s", randomWord)
                .toUriString();

        try {

            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode root = objectMapper.readTree(response.getBody());
            JsonNode searchResults = root.get("Search");

            if (searchResults != null && searchResults.isArray() && !searchResults.isEmpty()) {

                int randomIndex = new Random().nextInt(searchResults.size());
                JsonNode randomMovie = searchResults.get(randomIndex);
                String imdbID = randomMovie.get("imdbID").asText();

                String fullUrl = String.format("%s/?apikey=%s&i=%s", OMDB_API_URL, apiKey, imdbID);

                log.info("FullUrl {}", fullUrl);
                ResponseEntity<Movie> fullMovieResponse = restTemplate.getForEntity(fullUrl, Movie.class);

                return fullMovieResponse.getBody();
            } else {
                return null;
            }
        } catch (HttpClientErrorException | HttpServerErrorException | IOException e) {

            log.info(url);
            log.error("Ocorreu um erro: ", e);
            return null;
        }
    }

    private String getRandomWordFromDictionary() {
        Random random = new Random();
        int index = random.nextInt(DICTIONARY_WORDS.size());
        return DICTIONARY_WORDS.get(index);
    }
}
