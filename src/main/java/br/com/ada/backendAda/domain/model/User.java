package br.com.ada.backendAda.domain.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@Entity(name = "aut_user")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private UUID code;

    @Column(nullable = false)
    private String name;


    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(name = "total_quizzes", nullable = true)
    private Integer totalQuizzes;

    @Column(name = "correct_quizzes", nullable = true)
    private Integer correctQuizzes;

    private Double score;

    @ManyToMany
    @JoinTable(name = "aut_user_group", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "group_id"))
    private Set<Group> groups = new HashSet<>();
}
