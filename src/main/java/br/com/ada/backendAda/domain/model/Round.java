package br.com.ada.backendAda.domain.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@EqualsAndHashCode
@Entity(name = "ada_round")
public class Round {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "code", nullable = false)
    private String code;
    @ManyToOne
    private Game game;
    @ManyToOne
    private Movie movie1;
    @ManyToOne
    private Movie movie2;
    private boolean answered;
    private boolean correct;



}
