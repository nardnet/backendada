package br.com.ada.backendAda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendAdaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendAdaApplication.class, args);
	}

}
