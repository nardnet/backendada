CREATE TABLE ada_game (
                      id BIGINT NOT NULL AUTO_INCREMENT,
                      code VARCHAR(255) NOT NULL DEFAULT (UUID_TO_BIN(UUID(), true)),
                      user_id BIGINT,
                      active BOOLEAN NOT NULL,
                      correct_answers INT NOT NULL,
                      wrong_answers INT NOT NULL,
                      PRIMARY KEY (id),
                      UNIQUE (code),
                      FOREIGN KEY (user_id) REFERENCES aut_user(id) ON DELETE SET NULL
);