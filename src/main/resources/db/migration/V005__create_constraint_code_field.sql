alter table aut_user add constraint uk_user_code unique (code);
alter table aut_permission add constraint uk_permission_code unique (code);
alter table aut_group add constraint uk_group_code unique (code);
