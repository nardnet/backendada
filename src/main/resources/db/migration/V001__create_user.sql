create table aut_user (

                          id bigint not null auto_increment,
                          code binary(16) NOT NULL DEFAULT (UUID_TO_BIN(UUID(), true)),
                          name varchar(60) not null,
                          email varchar(320) not null,
                          password varchar(255) not null,
                          total_quizzes INT DEFAULT 0,
                          correct_quizzes INT DEFAULT 0,
                          score DOUBLE DEFAULT 0.0,

                          primary key (id)
);

