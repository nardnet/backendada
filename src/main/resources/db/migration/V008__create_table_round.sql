CREATE TABLE ada_round (
                       id BIGINT AUTO_INCREMENT PRIMARY KEY,
                       code VARCHAR(255) NOT NULL DEFAULT (UUID_TO_BIN(UUID(), true)),
                       game_id BIGINT,
                       movie1_id BIGINT,
                       movie2_id BIGINT,
                       answered BOOLEAN,
                       correct BOOLEAN,
                       FOREIGN KEY (game_id) REFERENCES ada_game (id),
                       FOREIGN KEY (movie1_id) REFERENCES ada_movie (id),
                       FOREIGN KEY (movie2_id) REFERENCES ada_movie (id)
);