package br.com.ada.backendAda.api.controller;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashSet;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GameControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private static final String TEST_USERNAME = "nardnet@ada.com";
    private static final String TEST_PASSWORD = "web123";

    private static final String TEST_USERNAME2 = "joao@ada.com";
    private static final String TEST_PASSWORD2 = "web123";

    private static final String TEST_USERNAME3 = "Paulo@ada.com";
    private static final String TEST_PASSWORD3 = "web123";

    private static String accessToken;
    private static String accessToken2;
    private static String accessToken3;

    private static Long gameId;
    private static Long roundId;

    private static Long gameId2;
    private static Long roundId2;

    private static Long gameId3;
    private static Long roundId3;

    @BeforeEach
    public void setup() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        String tokenEndpoint = "http://localhost:8081/oauth/token";  // Ajuste conforme necessário
        String clientID = "ada-web";
        String clientSecret = "web123";

        Response response = given()
                .auth()
                .preemptive()
                .basic(clientID, clientSecret)
                .formParam("grant_type", "password")
                .formParam("username", TEST_USERNAME)
                .formParam("password", TEST_PASSWORD)
                .post(tokenEndpoint);

        Response response1 = given()
                .auth()
                .preemptive()
                .basic(clientID, clientSecret)
                .formParam("grant_type", "password")
                .formParam("username", TEST_USERNAME2)
                .formParam("password", TEST_PASSWORD2)
                .post(tokenEndpoint);

        Response response2 = given()
                .auth()
                .preemptive()
                .basic(clientID, clientSecret)
                .formParam("grant_type", "password")
                .formParam("username", TEST_USERNAME3)
                .formParam("password", TEST_PASSWORD3)
                .post(tokenEndpoint);

        accessToken = response.jsonPath().getString("access_token");
        accessToken2 = response1.jsonPath().getString("access_token");
        accessToken3 = response2.jsonPath().getString("access_token");
    }

    @Test
    @Order(1)
    public void testStartGame() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/start")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("username", TEST_USERNAME)
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());

        String gameResponse = result.getResponse().getContentAsString();
        JSONObject gameJson = new JSONObject(gameResponse);
        gameId = gameJson.getLong("id");

        result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/start")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("username", TEST_USERNAME2)
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());

        gameResponse = result.getResponse().getContentAsString();
        gameJson = new JSONObject(gameResponse);
        gameId2 = gameJson.getLong("id");

        result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/start")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("username", TEST_USERNAME3)
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());

        gameResponse = result.getResponse().getContentAsString();
        gameJson = new JSONObject(gameResponse);
        gameId3 = gameJson.getLong("id");

    }

    @Test
    @Order(2)
    public void testGetNewRound() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/v1/games/round")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("gameId", gameId.toString())
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());

        String roundResponse = result.getResponse().getContentAsString();
        JSONObject roundJson = new JSONObject(roundResponse);
        roundId = roundJson.getLong("id");

        result = mockMvc.perform(MockMvcRequestBuilders.get("/v1/games/round")
                        .header("Authorization", "Bearer " + accessToken2)
                        .param("gameId", gameId2.toString())
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());

        roundResponse = result.getResponse().getContentAsString();
        roundJson = new JSONObject(roundResponse);
        roundId2 = roundJson.getLong("id");


        result = mockMvc.perform(MockMvcRequestBuilders.get("/v1/games/round")
                        .header("Authorization", "Bearer " + accessToken3)
                        .param("gameId", gameId3.toString())
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());

        roundResponse = result.getResponse().getContentAsString();
        roundJson = new JSONObject(roundResponse);
        roundId3 = roundJson.getLong("id");
    }

    @Test
    @Order(3)
    public void testAnswerRound() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/round/answer")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("roundId", roundId.toString())
                        .param("movieId", "1")
                        .param("username", TEST_USERNAME)
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());

        result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/round/answer")
                        .header("Authorization", "Bearer " + accessToken2)
                        .param("roundId", roundId2.toString())
                        .param("movieId", "4")
                        .param("username", TEST_USERNAME2)
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());


        result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/round/answer")
                        .header("Authorization", "Bearer " + accessToken3)
                        .param("roundId", roundId3.toString())
                        .param("movieId", "6")
                        .param("username", TEST_USERNAME3)
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());


    }

    @Test
    @Order(4)
    public void testEndGame() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/end")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("gameId", gameId.toString())
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());


       result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/end")
                        .header("Authorization", "Bearer " + accessToken2)
                        .param("gameId", gameId2.toString())
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());

        result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/end")
                        .header("Authorization", "Bearer " + accessToken3)
                        .param("gameId", gameId3.toString())
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());
    }

    @Test
    @Order(5)
    public void testGetNewRound_ValidPairs() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/start")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("username", TEST_USERNAME)
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());

        String gameResponse = result.getResponse().getContentAsString();
        JSONObject gameJson = new JSONObject(gameResponse);
        gameId = gameJson.getLong("id");

        MvcResult newRoundResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/games/round")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("gameId", gameId.toString())
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        // Verifique se os pares de filmes são válidos
        String roundResponse = newRoundResult.getResponse().getContentAsString();
        JSONObject roundJson = new JSONObject(roundResponse);

        JSONObject movie1 = roundJson.getJSONObject("movie1");
        JSONObject movie2 = roundJson.getJSONObject("movie2");

        JSONArray moviesJson = new JSONArray();
        moviesJson.put(movie1);
        moviesJson.put(movie2);

        // Verifique cada par de filmes na rodada
        Set<String> moviePairs = new HashSet<>();
        for (int i = 0; i < moviesJson.length(); i++) {
            JSONObject movieJson = moviesJson.getJSONObject(i);
            String movieId = movieJson.getString("id");

            // Verifique se o filme já foi usado no par anteriormente
            if (moviePairs.contains(movieId)) {
                fail("Pairs contain repeated movie: " + movieId);
            } else {
                moviePairs.add(movieId);
            }
        }

        // Verifique se todos os pares de filmes são únicos
        assertEquals(moviesJson.length(), moviePairs.size(), "Pairs contain repeated movies");

        result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/end")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("gameId", gameId.toString())
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());
    }


    @Test
    @Order(6)
    public void testGetRanking() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/v1/games/ranking")
                        .header("Authorization", "Bearer " + accessToken)
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        log.info(contentAsString);
        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());
    }

    @Test
    @Order(7)
    public void testInvalidEndpoint() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/v1/games/nonexistent")
                        .header("Authorization", "Bearer " + accessToken)
                        .accept("application/json"))
                .andReturn();

        assertEquals(404, result.getResponse().getStatus());

    }

    @Test
    @Order(8)
    public void testCannotProceedWithoutAnsweringCurrentRound() throws Exception {
        // Start a new game
        MvcResult startGameResult = mockMvc.perform(MockMvcRequestBuilders.post("/v1/games/start")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("username", TEST_USERNAME)
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();
        assertEquals(200, startGameResult.getResponse().getStatus());

        // Extract gameId from the response
        String gameResponse = startGameResult.getResponse().getContentAsString();
        JSONObject gameJson = new JSONObject(gameResponse);
        Long newGameId = gameJson.getLong("id");

        // Request a new round
        MvcResult newRoundResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/games/round")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("gameId", newGameId.toString())
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();
        assertEquals(200, newRoundResult.getResponse().getStatus());

        // Try to get another round without answering the current one
        MvcResult secondRoundResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/games/round")
                        .header("Authorization", "Bearer " + accessToken)
                        .param("gameId", newGameId.toString())
                        .contentType("application/json")
                        .accept("application/json"))
                .andReturn();

        // Ensure that the request fails due to unanswered current round
        assertEquals(400, secondRoundResult.getResponse().getStatus());
    }


}
